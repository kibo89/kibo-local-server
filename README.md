# Kibo Local Server
Local Server for [Kibo React Theme](https://gitlab.com/sudhirdhumal289/kibo-react-core-theme)

#### TODO:
1. Support for Kibo specific API endpoints
2. Support for Hypr tags listed on Kibo Documentation page
3. Support for Hypr filters listed on Kibo Documentation page

Personally I don't recommend you to use the Hypr specific tags or filters,
but still I'm going to add them from compatibility perspective.


#### NOTE:
Please make sure you deploy your code on Kibo theme and test it thoroughly before publishing it on QA, Production or any non-developer sandbox.
