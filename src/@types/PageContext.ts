export type User = {
  isAuthenticated: boolean;
  userId: string;
  firstName: string;
  lastName: string;
  email: string;
  isAnonymous: boolean;
  behaviors: number[];
};

export type UserProfile = {
  userId: string;
  firstName: string;
  lastName: string;
  emailAddress: string;
  userName: string;
};

export type UserVisitDetails = {
  visitId: string;
  visitorId: string;
  isTracked: boolean;
  isUserTracked: boolean;
};

export type CrawlerInfo = { isCrawler: boolean };

export type PageContext = {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  numberFormat?: any;
  correlationId: string;
  ipAddress: string;
  isDebugMode: boolean;
  isCrawler: boolean;
  isMobile: boolean;
  isTablet: boolean;
  isDesktop: boolean;
  visit: UserVisitDetails;
  user: User;
  userProfile: UserProfile;
  isEditMode: boolean;
  isAdminMode: boolean;
  now: string;
  crawlerInfo: CrawlerInfo;
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  currencyRateInfo: any;
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  query?: any;
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [property: string]: any;
};
