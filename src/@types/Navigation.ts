export type NavigationNode = {
  url: string;
  name: string;
  categoryCode: string;
  nodeType: string;
  isHomePage: boolean;
  expanded: boolean;
  expandable: boolean;
  isLeaf: boolean;
  isEmpty: boolean;
  fqUrl: string;
  items?: NavigationNode[];
};

export type Breadcrumb = {
  url: string;
  name: string;
  nodeType: string;
};

export type Navigation = {
  currentNode: NavigationNode;
  breadcrumbs: Breadcrumb[];
  tree: NavigationNode[];
};
