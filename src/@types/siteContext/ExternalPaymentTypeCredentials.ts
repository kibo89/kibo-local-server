export type ExternalPaymentTypeCredentials = {
  displayName: string;
  apiName: string;
  inputType: string;
  vocabularyValues: [];
};
