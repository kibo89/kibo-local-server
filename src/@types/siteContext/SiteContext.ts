import { KeyValueObject } from '../KeyValueObject';
import { GeneralSettings } from './GeneralSettings';
import { CheckoutSettings } from './CheckoutSettings';
import { DomainSettings } from './DomainSettings';
import { CurrencyInfo } from './CurrencyInfo';

export type Domains = {
  current: DomainSettings;
  primary: DomainSettings;
  all: DomainSettings[];
};

export type SiteContext = {
  siteExists: boolean;
  tenantId: number;
  siteId: number;
  hashString: string;
  labels: KeyValueObject;
  themeId: string;
  generalSettings: GeneralSettings;
  checkoutSettings: CheckoutSettings;
  themeSettings: KeyValueObject;
  isEditMode: boolean;
  cdnPrefix: string;
  secureHost: string;
  supportsInStorePickup: boolean;
  domains: Domains;
  currencyInfo: CurrencyInfo;
  siteSubdirectory?: string;
};
