import { StrKeyValObject } from '../StrKeyValObject';
import { SupressedEmailTransactions } from './SupressedEmailTransactions';
import { EmailType } from './EmailType';

export type GeneralSettings = {
  websiteName: string;
  siteTimeZone: string;
  siteTimeFormat: string;
  adjustForDaylightSavingTime: boolean;
  allowAllIPs: boolean;
  senderEmailAddress: string;
  replyToEmailAddress: string;
  supressedEmailTransactions: SupressedEmailTransactions;
  desktopTheme: StrKeyValObject;
  tabletTheme: StrKeyValObject;
  isGoogleAnalyticsEnabled: boolean;
  isGoogleAnalyticsEcommerceEnabled: boolean;
  isWishlistCreationEnabled: boolean;
  isMultishipEnabled: boolean;
  allowInvalidAddresses: boolean;
  isAddressValidationEnabled: boolean;
  isRequiredLoginForLiveEnabled: boolean;
  isRequiredLoginForStagingEnabled: boolean;
  customCdnHostName: string;
  emailTypes: EmailType[];
  enforceSitewideSSL: boolean;
  cdnCacheBustKey?: string;
};
