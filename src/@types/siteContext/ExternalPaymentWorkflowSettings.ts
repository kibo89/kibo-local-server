import { ExternalPaymentTypeCredentials } from './ExternalPaymentTypeCredentials';

export type ExternalPaymentWorkflowSettings = {
  name: string;
  namespace: string;
  fullyQualifiedName: string;
  isEnabled: boolean;
  credentials: ExternalPaymentTypeCredentials[];
};
