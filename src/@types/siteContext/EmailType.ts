/* eslint-disable @typescript-eslint/no-explicit-any */
export type EmailType = {
  enabled: any;
  id: string;
  senderEmailAddressOverride: any;
  senderEmailAliasOverride: any;
  replyToEmailAddressOverride: any;
  bccEmailAddressOverride: any;
  onlyOnApiRequest: any;
};
