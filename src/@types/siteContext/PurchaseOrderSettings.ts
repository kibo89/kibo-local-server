export type PurchaseOrderSettings = {
  isEnabled: boolean;
  allowSplitPayment: boolean;
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  customFields: any[];
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  paymentTerms: any[];
};
