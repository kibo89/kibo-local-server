export type CurrencyInfo = {
  currencyCode: number;
  englishName: string;
  symbol: string;
  precision: number;
  roundingType: number | string;
};
