import { VisaCheckoutSettings } from './VisaCheckoutSettings';
import { PurchaseOrderSettings } from './PurchaseOrderSettings';
import { ExternalPaymentWorkflowSettings } from './ExternalPaymentWorkflowSettings';

export type CheckoutSettings = {
  customerCheckoutType: string;
  paymentProcessingFlowType: string;
  payByMail: boolean;
  useOverridePriceToCalculateDiscounts: boolean;
  isPayPalEnabled: boolean;
  purchaseOrder: PurchaseOrderSettings;
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  supportedCards: any;
  visaCheckout: VisaCheckoutSettings;
  externalPaymentWorkflowSettings: ExternalPaymentWorkflowSettings[];
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  supportedGiftCards: any;
};
