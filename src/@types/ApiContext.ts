import { StrKeyValObject } from './StrKeyValObject';

export type ApiContext = {
  headers: StrKeyValObject;
  urls: StrKeyValObject;
};
