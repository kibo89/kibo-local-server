export type StrKeyValObject = {
  [property: string]: string;
};
