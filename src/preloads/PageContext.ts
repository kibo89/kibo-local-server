import { PageContext } from '../@types/PageContext';

const pageContext: PageContext = {
  correlationId: '5136139b6fb94439b9152e01c239ab5f',
  ipAddress: '106.77.6.6',
  isDebugMode: false,
  isCrawler: false,
  isMobile: false,
  isTablet: false,
  isDesktop: true,
  visit: {
    visitId: 'wfSxwyOjsk2wl5eQpZXMZQ',
    visitorId: 'Mda83O5oSE2BIjMDXD3I_Q',
    isTracked: false,
    isUserTracked: false,
  },
  user: {
    isAuthenticated: false,
    userId: '131ce99e73ea43c1942b1f5b443502f8',
    firstName: '',
    lastName: '',
    email: '',
    isAnonymous: true,
    behaviors: [1014, 222],
  },
  userProfile: {
    userId: '131ce99e73ea43c1942b1f5b443502f8',
    firstName: '',
    lastName: '',
    emailAddress: '',
    userName: '',
  },
  isEditMode: false,
  isAdminMode: false,
  now: '2020-12-20T05:57:34.1053685Z',
  crawlerInfo: { isCrawler: false },
  currencyRateInfo: {},
};

export default pageContext;
