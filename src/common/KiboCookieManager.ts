import express from 'express';
import pageContext from '../preloads/PageContext';

const kiboCookieManager = express.Router();

kiboCookieManager.all('*', (req, resp, next) => {
  // Add mzpc cookie
  const mzpcValue = Buffer.from(JSON.stringify(pageContext), 'utf8').toString('base64');
  resp.cookie('_mzPc', mzpcValue, { path: '/', httpOnly: false });

  next();
});

export default kiboCookieManager;
