import https from 'https';
import http from 'http';
import express, { Express } from 'express';
import path from 'path';
import cookieParser from 'cookie-parser';
import fs from 'fs';
import ErrnoException = NodeJS.ErrnoException;

import config from '../config/appConfig.json';
import ViewMapper from './ViewMapper';
import kiboCookieManager from './common/KiboCookieManager';

class WebServer {
  expressApp: Express;
  httpPort = config.httpPort || 8080;
  httpsPort = config.httpsPort || 8443;
  themePath: string;

  constructor() {
    this.expressApp = express();
    this.themePath = process.cwd();

    this.setupMiddlewares();
    new ViewMapper(this.expressApp);

    this.startServer();
  }

  /**
   * Register all the middlewares
   */
  setupMiddlewares(): void {
    this.expressApp.use(express.json());
    this.expressApp.use(express.urlencoded({ extended: false }));
    this.expressApp.use(cookieParser());

    this.expressApp.all('*', kiboCookieManager);

    this.expressApp.use(express.static(path.join(__dirname, '..', 'public')));
    this.expressApp.use('/hscripts', express.static(path.join(__dirname, '..', 'kibo-hosted-resources')));
    this.expressApp.use('/lscripts', express.static(path.join(this.themePath, 'kibo-core', 'scripts')));
    this.expressApp.use('/scripts', express.static(path.join(this.themePath, 'build')));
    this.expressApp.use('/resources', express.static(path.join(this.themePath, 'build')));
    this.expressApp.use('/stylesheets', express.static(path.join(this.themePath, 'build')));
    this.expressApp.use(`/js`, express.static(path.join(__dirname, '..', 'kibo-hosted-resources')));
  }

  /**
   * Start HTTP & HTTPS servers on port 8080 & 8443 respectively
   */
  startServer(): void {
    this.expressApp.set('port', this.httpPort);

    const httpServer = http.createServer(this.expressApp);
    httpServer.listen(this.httpPort);
    httpServer.on('error', this.onError);
    httpServer.on('listening', () => console.log(`Listening on HTTP: ${this.httpPort}`));

    // Create HTTPS server
    const httpsOptions = {
      key: fs.readFileSync(path.join(__dirname, '..', 'certs', 'key.pem')),
      cert: fs.readFileSync(path.join(__dirname, '..', 'certs', 'cert.pem')),
    };
    const httpsServer = https.createServer(httpsOptions, this.expressApp).listen(this.httpsPort);
    httpsServer.on('error', this.onError);
    httpServer.on('listening', () => console.log(`Listening on HTTPS: ${this.httpsPort}`));
  }

  /**
   * Handle and print errors occurred while starting the HTTP & HTTPS serves.
   *
   * @param error Error thrown by server on startup.
   */
  onError(error: ErrnoException): void {
    if (error.syscall !== 'listen') {
      throw error;
    }

    // handle specific listen errors with friendly messages
    switch (error.code) {
      case 'EACCES':
        // eslint-disable-next-line no-console
        console.error(`Either of ${this.httpPort} or ${this.httpsPort} port requires elevated privileges`);
        break;
      case 'EADDRINUSE':
        // eslint-disable-next-line no-console
        console.error(`Either ${this.httpPort} or ${this.httpsPort} port is already in use`);
        break;
      default:
        throw error;
    }

    process.exit(1);
  }
}

export default new WebServer();
