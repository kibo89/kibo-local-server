import { Express } from 'express';
import * as path from 'path';
import Hypr from './hypr/Hypr';
import urlPageTypeMapping from '../config/urlPageTypeMapping.json';
import * as fs from 'fs';

export default class ViewMapper {
  expressApp: Express;
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  themeConfig: any;

  constructor(expressApp: Express) {
    this.expressApp = expressApp;
    this.themeConfig = JSON.parse(fs.readFileSync(path.join(process.cwd(), 'kibo-core', 'theme.json')).toString());

    this.registerViewEngine();
    this.registerOOBRoutes();
  }

  registerViewEngine(): void {
    this.expressApp.engine('hypr', (filePath, options, callback) => {
      try {
        const parsedHypr = Hypr.getTemplate(filePath);

        callback(null, parsedHypr.render());
      } catch (err) {
        callback(err);
      }
    });

    this.expressApp.set('views', path.join(process.cwd(), 'kibo-core', 'templates', 'pages'));
    this.expressApp.set('view engine', 'hypr');
  }

  registerOOBRoutes(): void {
    urlPageTypeMapping.forEach((mappingEntry) => {
      // eslint-disable-next-line @typescript-eslint/no-explicit-any
      const pageTypeConfig = this.themeConfig.pageTypes.find((pageType: any) => pageType.id === mappingEntry.pageId);

      if (pageTypeConfig && pageTypeConfig.template) {
        this.expressApp.get(mappingEntry.url, function (req, res) {
          res.render(pageTypeConfig.template);
        });
      }
    });

    // 404
    this.expressApp.get('*', function (req, res) {
      res.render('home');
    });
  }
}
