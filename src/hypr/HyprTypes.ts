import { Swig } from 'swig';
import { SiteContext } from '../@types/siteContext/SiteContext';
import { KeyValueObject } from '../@types/KeyValueObject';
import { ApiContext } from '../@types/ApiContext';
import { PageContext, User } from '../@types/PageContext';

export type Hypr = {
  engine: Swig;
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  getTemplate: (path: string) => any;
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  getThemeSetting: (setting: string) => any;
  getLabel: (name: string) => string | undefined;
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  immanentize: () => any;
};

export type HyperContextLocals = {
  now: string;
  pageContext: PageContext;
  apiContext: ApiContext;
  user: User;
  themeSettings: KeyValueObject;
  siteContext: SiteContext;
  labels: KeyValueObject;
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [property: string]: any;
};

export type HyprContext = {
  locals: HyperContextLocals;
};
