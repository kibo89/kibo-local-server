/* eslint-disable @typescript-eslint/no-explicit-any */
import { Swig, TemplateLoader } from 'swig';
import _ from 'lodash';
import fs from 'fs';
import * as path from 'path';

import { HyprContext, Hypr as HyprType } from './HyprTypes';
import LocalFileLoader from './LocalFileLoader';

import apiContext from '../preloads/ApiContext';
import siteContext from '../preloads/SiteContext';
import pageContext from '../preloads/PageContext';
import navigation from '../preloads/Navigation';

import { KeyValueObject } from '../@types/KeyValueObject';
import { CurrencyInfo } from '../@types/siteContext/CurrencyInfo';

// Tags introduced to compile hypr file
import PreloadJSON from './tags/PreloadJSON';
import RequireScript from './tags/RequireScript';
import FirstOf from './tags/FirstOf';
import PartialCache from './tags/PartialCache';

// Tags extracted in separate file.
import UnSupported from './tags/UnSupportedTags';
import Comment from './tags/Comment';
import Dump from './tags/Dump';
import With from './tags/With';
import Dropzone from './tags/Dropzone';

const themeJson: any = JSON.parse(fs.readFileSync(path.join(process.cwd(), 'kibo-core', 'theme.json')).toString());

const hyprContext: HyprContext = {
  locals: {
    now: new Date().toISOString(),
    pageContext: pageContext,
    apiContext: apiContext,
    user: pageContext.user,
    themeSettings: _.merge(themeJson.settings, siteContext.themeSettings) || {},
    siteContext: siteContext,
    navigation: navigation,
    labels: {},
  },
};

const Hypr: HyprType = {
  engine: new Swig({
    cache: false,
    cmtControls: ['{% comment %}', '{% endcomment %}'],
    locals: hyprContext.locals,
    loader: new LocalFileLoader(),
  }),
  getTemplate: getHyprTemplate,
  getThemeSetting: function (setting: string): any {
    return hyprContext.locals.themeSettings[setting];
  },
  getLabel: function (name: string): string | undefined {
    if (arguments.length === 1) {
      return hyprContext.locals.labels[name];
    }
    if (arguments.length > 1) {
      // eslint-disable-next-line prefer-rest-params
      return formatString(hyprContext.locals.labels[name], Array.prototype.slice.call(arguments, 1));
    }
  },
  immanentize: function (): any | undefined {
    if (hyprContext.locals.pageContext) {
      hyprContext.locals.pageContext.query = deparam();
    }
  },
};
Hypr.immanentize();

class HyprTemplate {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  precompiledTpl: any;
  path: string;

  constructor(precompiledTpl: any, templatePath: string) {
    this.precompiledTpl = precompiledTpl;
    this.path = templatePath;
  }

  render(obj?: any) {
    Hypr.immanentize();
    return Hypr.engine.run(this.precompiledTpl.tpl, obj, this.path);
  }
}

interface SwigOptions {
  autoescape?: boolean;
  cache?: any;
  cmtControls?: string[];
  loader?: TemplateLoader;
  locals?: any;
  tagControls?: string[];
  varControls?: string[];
  filename: string;
}

function getHyprTemplate(templatePath: string): HyprTemplate {
  const tptText = fs.readFileSync(templatePath).toString();

  return new HyprTemplate(
    Hypr.engine.precompile(tptText, {
      filename: templatePath,
    } as SwigOptions),
    templatePath,
  );
}

function formatString(str: string, arr: string[]): string {
  let formatted = str;
  const otherArgs = arr;

  for (let i = 0, len = otherArgs.length; i < len; i++) {
    formatted = formatted.split('{' + i + '}').join(otherArgs[i] || '');
  }

  return formatted;
}

function deparam(querystring = '') {
  // remove any preceding url and split
  const localQuerystring = querystring.substring(querystring.indexOf('?') + 1).split('&');
  const params: KeyValueObject = {};
  let pair;

  // march and parse
  for (let i = localQuerystring.length; i > 0; ) {
    pair = localQuerystring[--i].split('=');
    params[decodeURIComponent(pair[0])] = decodeURIComponent(pair[1]);
  }

  return params;
}

const MakeUrlTag = {
  parse: function (str?: string, line?: string, parser?: any, types?: any) {
    let withEncountered = false;
    let asEncountered = false;
    let key: boolean;

    parser.on(types.VAR, function (this: any, token: any) {
      if (token.match === 'with') {
        withEncountered = true;
        return false;
      }
      if (token.match === 'as_parameter') {
        asEncountered = true;
        return false;
      }
      if (withEncountered && !asEncountered) {
        if (!key) {
          key = true;
          this.out.push('"' + token.match + '"');
          return false;
        } else {
          key = false;
        }
        return true;
      }
      return true;
    });

    return true;
  },
  compile: function (compiler?: any, args?: string[]) {
    return '_output += _ext.makeUrlTag(' + args + ');';
  },
};
Hypr.engine.setExtension('makeUrlTag', function (type: string, object: any) {
  // eslint-disable-next-line prefer-rest-params
  const params = Array.prototype.slice.call(arguments, 2);
  let query = '?';
  const fnName = type + 'Url';
  let secondArg;

  for (let i = 0; i < params.length; i++) {
    if (type === 'paging' && params[i - 1] === 'page') {
      secondArg = params[i];
    } else if (type === 'product' && params[i - 1] === 'variant') {
      secondArg = params[i];
    } else if ((type !== 'paging' && type !== 'product') || (params[i] !== 'page' && params[i] !== 'variant')) {
      query += (i % 2 ? '=' : '&') + encodeURI(params[i]);
    }
  }

  if (typeof util[fnName] === 'function') {
    return util[fnName](object, query, secondArg);
  }

  return '#';
});
Hypr.engine.setTag('make_url', MakeUrlTag.parse, MakeUrlTag.compile, false, false);

const util: KeyValueObject = {
  sortingKey: 'sortBy',
  facetKey: 'facetValueFilter',
  inStockLocationKey: 'inStockLocationKey',
  pagingKey: 'startIndex',
  imageUrl: function (object: any, extendedQuery: string) {
    const url = typeof object === 'object' ? object.imageUrl : object;
    return this.urlScrub(url + extendedQuery + this.getCdnCacheBust());
  },
  inStockLocationUrl: function (object: any, extendedQuery: string) {
    const query = this.parseQuery();
    query[this.inStockLocationKey] = object;
    return this.urlScrub(extendedQuery + '&' + this.stringify(query));
  },
  productUrl: function (object: any, extendedQuery: string, variant: string) {
    const code = typeof object === 'object' ? object.productCode : object;
    if (!variant) {
      return this.urlScrub('/p/' + code + extendedQuery);
    }
    return this.urlScrub('/p/' + code + '/v/' + variant + extendedQuery);
  },
  categoryUrl: function (object: any, extendedQuery: string) {
    const code = typeof object === 'object' ? object.categoryCode : object;
    return this.urlScrub('/c/' + code + extendedQuery);
  },
  sortingUrl: function (object: any, extendedQuery: string) {
    const query = this.parseQuery();
    query[this.sortingKey] = object;
    return this.urlScrub(extendedQuery + '&' + this.stringify(query));
  },
  facetUrl: function (object: any, extendedQuery: string) {
    const filterValue = typeof object === 'object' ? object.filterValue : object,
      query = this.parseQuery(),
      filters = !query[this.facetKey] ? [] : query[this.facetKey].split(','),
      idx = filters.indexOf(filterValue);

    if (idx === -1) {
      filters.push(filterValue);
    } else {
      filters.splice(idx, 1);
    }

    query[this.facetKey] = filters.join(',');

    return this.urlScrub(extendedQuery + '&' + this.stringify(query));
  },
  cdnUrl: function (object: any, extendedQuery: string) {
    const url = object + extendedQuery + this.getCdnCacheBust();
    return this.urlScrub(hyprContext.locals.siteContext.cdnPrefix + url);
  },
  pagingUrl: function (object: any, extendedQuery: string, page: string) {
    let startIndex = 0;
    const query = this.parseQuery();
    const max = Math.floor(object.totalCount / object.pageSize) * object.pageSize;

    if (typeof page === 'undefined') {
      page = '' + (object.startIndex / object.pageSize + 1);
    }

    switch (page) {
      case 'first':
        startIndex = 0;
        break;
      case 'last':
        startIndex = max;
        break;
      case 'previous':
        startIndex = object.startIndex - object.pageSize;
        break;
      case 'next':
        startIndex = object.startIndex + object.pageSize;
        break;

      default:
        // eslint-disable-next-line no-case-declarations
        const localPage = parseInt(page);
        if (!isNaN(localPage)) {
          startIndex = (localPage - 1) * object.pageSize;
        }
    }

    if (startIndex < 0) {
      startIndex = 0;
    } else if (startIndex > max) {
      startIndex = max;
    }

    query[this.pagingKey] = startIndex;

    return this.urlScrub(extendedQuery + '&' + this.stringify(query));
  },
  indexOf: function (arr: any[], item: any) {
    return arr.indexOf(item);
  },
  map: function (arr: any[], fn: (el: any) => any) {
    return arr.map(fn);
  },
  parseQuery: function (str = '') {
    const mapArray = str.replace(/(^\?)/, '').split('&');
    const ret = mapArray.map(
      function (this: any, n: any) {
        n = n.split('=');
        this[decodeURIComponent(n[0])] = decodeURIComponent(n[1]);
        return this;
      }.bind({}),
    )[0];
    delete ret[''];
    return ret;
  },
  stringify: function (obj: any) {
    const str: string[] = [];
    for (const p in obj) {
      // eslint-disable-next-line no-prototype-builtins
      if (obj.hasOwnProperty(p)) {
        str.push(encodeURIComponent(p) + '=' + encodeURIComponent(obj[p]));
      }
    }

    return str.join('&');
  },
  getCdnCacheBust: function () {
    return '&_mzcb=' + hyprContext.locals.siteContext.generalSettings.cdnCacheBustKey;
  },
  urlScrub: function (url: string) {
    let stem = url
      .replace(/(&$)|(\?$)/g, '')
      .replace(/\?&/, '?')
      .replace(/&+/g, '&');
    if (stem.length > 2 && stem[0] === '/' && stem[1] !== '/') {
      stem = (hyprContext.locals.siteContext.siteSubdirectory || '') + stem;
    }
    return stem;
  },
};

function formatMoney(
  n: number | string,
  decPlaces: number,
  thouSeparator: string | null,
  decSeparator: string | null,
  symbol: string,
  positivePattern: number,
  negativePattern: number,
  roundUp: any,
  conversionRate: number,
  conversionRateRounding: number,
) {
  let j, patterns, pattern, returnString;
  decPlaces = isNaN((decPlaces = Math.abs(decPlaces))) ? 2 : decPlaces;
  const om = Math.pow(10, decPlaces);

  symbol = symbol || '$';
  decSeparator = decSeparator == undefined ? '.' : decSeparator;
  thouSeparator = thouSeparator == undefined ? ',' : thouSeparator;
  n = Number(n) * conversionRate;
  const isNegative = n < 0;
  if (!isNaN(conversionRateRounding)) {
    n =
      Math.round(Math.abs(+n || 0) * Math.pow(10, conversionRateRounding)) * Math.pow(10, -1 * conversionRateRounding);
  }
  n = n + '';
  const i: string = parseInt(String((n = Math.round(om * Math.abs(+n || 0)) / om)), 10) + '';
  j = (j = i.length) > 3 ? j % 3 : 0;
  const s =
    (j ? i.substr(0, j) + thouSeparator : '') +
    i.substr(j).replace(/(\d{3})(?=\d)/g, '$1' + thouSeparator) +
    (decPlaces
      ? decSeparator +
        Math.abs(n - parseInt(i))
          .toFixed(decPlaces)
          .slice(2)
      : '');

  if (!isNegative) {
    patterns = ['$n', 'n$', '$ n', 'n $'];
    pattern = patterns[positivePattern];
  } else {
    patterns = [
      '($n)',
      '-$n',
      '$-n',
      '$n-',
      '(n$)',
      '-n$',
      'n-$',
      'n$-',
      '-n $',
      '-$ n',
      'n $-',
      '$ n-',
      '$ -n',
      'n- $',
      '($ n)',
      '(n $)',
    ];
    pattern = patterns[negativePattern];
  }
  returnString = pattern.replace('n', s);
  returnString = returnString.replace('$', symbol);

  return returnString;
}

const decimalPlacesRE = /\.\d+/;

const MAX_PLACES = 10;

function getPrecision(num: number) {
  if (isNaN(num) || parseInt(`${num}`) === num) return 0;
  const m = num.toString().match(decimalPlacesRE);
  return (m && m[0].length) || 0;
}

function getHighestPrecision(nums: []) {
  return Math.min(
    Math.max(
      nums.reduce(function (highest, num) {
        return Math.max(highest, getPrecision(num));
      }, 0),
      2,
    ),
    15,
  );
}

function roundToPrecision(num: number, precision: number, down: any) {
  const c = Math.pow(10, Math.min(precision, MAX_PLACES));
  return Math[down ? (num < 0 ? 'ceil' : 'floor') : 'round'](c * num) / c;
}

function ensureNumeric(fn: any, forcePrecision?: any) {
  const useForcePrecision = arguments.length === 2;
  return function (): string {
    // eslint-disable-next-line prefer-rest-params
    const argsInner = Array.prototype.map.call(arguments, Number);
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore
    const precision = useForcePrecision ? forcePrecision : getHighestPrecision(argsInner);
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore
    return roundToPrecision(fn.apply(this, argsInner), precision);
  };
}

function MaybeDate(v: any) {
  // interpret a number or a number string as a seconds value
  const nv = Number(v);
  let d;
  if (isNaN(nv)) {
    d = new Date(v);
  } else {
    if (v instanceof Date) {
      d = v;
    } else {
      d = new Date(v * 1000);
    }
  }
  return isNaN(+d) ? null : d;
}

function ensureDate(fn: any) {
  return function (this: any) {
    // eslint-disable-next-line prefer-rest-params
    const args = Array.prototype.map.call(arguments, MaybeDate);
    return fn.apply(this, args);
  };
}

let currencyInfo: CurrencyInfo;
const RoundingTypeConst = {
  UpToCurrencyPrecision: 'upToCurrencyPrecision',
};

Hypr.engine.setFilter('currency', function (num: number, useCulturalNegativeStandard: boolean, symbol: string) {
  let conversionRate;
  let conversionRateRounding;
  if (!currencyInfo) {
    try {
      currencyInfo = hyprContext.locals.siteContext.currencyInfo;
    } catch (e) {
      // ignore
    }
    currencyInfo = currencyInfo || {
      symbol: '$',
      precision: 2,
      roundingType: 'upToCurrencyPrecision',
    };
  }
  if (!conversionRate) {
    try {
      conversionRate = (hyprContext.locals.pageContext.currencyRateInfo || {}).rate || 1;
      conversionRateRounding = (hyprContext.locals.pageContext.currencyRateInfo || {}).rounding;
    } catch (e) {
      conversionRate = 1;
    }
  }

  let positivePattern = 0;
  let negativePattern = 1;
  const numberFormat = hyprContext.locals.pageContext.numberFormat;

  /*
   It turns out that negative currency values can be displayed in lots of weird little
   ways that we didn't prepare for when writing core theme. We want to give our clients
   the option to use this special formatting without breaking/significantly changing
   their existing themes. This is why we include the useCulturalNegativeStandard flag; if
   it is TRUE, the filter will use whichever CurrencyNegativePattern is saved in the page
   context for the current localecode. If it is false, we default to the pattern
   "-$n", which is the number 1.
   */

  if (numberFormat) {
    positivePattern = numberFormat.currencyPositivePattern;
    if (useCulturalNegativeStandard) {
      negativePattern = numberFormat.currencyNegativePattern;
    }
  }
  return formatMoney(
    num,
    currencyInfo.precision,
    null,
    null,
    symbol || currencyInfo.symbol,
    positivePattern,
    negativePattern,
    currencyInfo.roundingType === RoundingTypeConst.UpToCurrencyPrecision,
    conversionRate,
    conversionRateRounding,
  );
});

Hypr.engine.setFilter('divisibleby', function (num: any, divisor: any) {
  return num && num % divisor === 0;
});

function divide(num: any, divisor: any) {
  return num / divisor;
}

function add(num: any, addend: any) {
  return num + addend;
}

function subtract(num: any, amount: any) {
  return num - amount;
}

function multiply(num: any, term: any) {
  return num * term;
}

Hypr.engine.setFilter('divide', ensureNumeric(divide, 14));

Hypr.engine.setFilter('add', ensureNumeric(add));

Hypr.engine.setFilter('subtract', ensureNumeric(subtract));

Hypr.engine.setFilter('multiply', ensureNumeric(multiply, 14));

Hypr.engine.setFilter(
  'mod',
  ensureNumeric(function (num: any, term: any) {
    return num % term;
  }),
);

function floatFormat(num: number, places: number, omitIfRound: boolean, roundDown: boolean) {
  if (parseInt(`${num}`) === num && omitIfRound) return num;
  return roundToPrecision(num, places, roundDown).toFixed(places);
}

Hypr.engine.setFilter('floatformat', function (num: any, placesArg: string, roundingBehavior: string): string {
  const n = Number(num);
  let places = parseInt(`${Number(placesArg)}`);
  if (num === '' || isNaN(n)) return '';
  if (placesArg === undefined) places = -1;
  if (isNaN(places)) return num;
  return '' + floatFormat(n, Math.min(Math.abs(places), MAX_PLACES), places < 0, roundingBehavior === 'down');
});

Hypr.engine.setFilter('add_url_param', function (url: string, param: string, value: string) {
  return url + (url.indexOf('?') === -1 ? '?' : '&') + encodeURIComponent(param) + '=' + encodeURIComponent(value);
});

Hypr.engine.setFilter(
  'slugify',
  (function () {
    const trimRE = /^\s+|\s+$/g,
      invalidCharsRE = /[^a-z0-9 -]/g,
      collapseWhitespaceRE = /\s+/g,
      collapseDashRE = /-+/g,
      accentREs: RegExp[] = [],
      accentFrom = 'àáäâèéëêìíïîòóöôùúüûñç·/_,:;'.split(''),
      accentTo = 'aaaaeeeeiiiioooouuuunc------'.split('');

    for (let i = 0, l = accentFrom.length; i < l; i++) {
      accentREs[i] = new RegExp(accentFrom[i], 'g');
    }

    function string_to_slug(str: string) {
      str = str.toString().replace(trimRE, '').toLowerCase();

      for (let j = 0, k = accentFrom.length; j < k; j++) {
        str = str.replace(accentREs[j], accentTo[j]);
      }

      str = str
        .replace(invalidCharsRE, '-') // remove invalid chars
        .replace(collapseWhitespaceRE, '-') // collapse whitespace and replace by -
        .replace(collapseDashRE, '-'); // collapse dashes

      return str;
    }

    return string_to_slug;
  })(),
);

Hypr.engine.setFilter('truncatewords', function (str: string, num: number) {
  const words = str.split(' ');
  str = words.slice(0, num).join(' ');
  if (words.length > num) str += ' ...';
  return str;
});

Hypr.engine.setFilter('urlencode', function (str: string) {
  return encodeURIComponent(str.toString());
});

Hypr.engine.setFilter('string_format', function (tpt: string) {
  let formatted = tpt;
  // eslint-disable-next-line prefer-rest-params
  const otherArgs = Array.prototype.slice.call(arguments, 1);
  for (let i = 0, len = otherArgs.length; i < len; i++) {
    formatted = formatted.split('{' + i + '}').join(otherArgs[i]);
  }
  return formatted;
});

function prop(o: any, pn: string, caseSensitive: boolean): any {
  if (o) {
    if (caseSensitive) return o[pn];
    pn = pn.toLowerCase();
    for (const k in o) {
      if (Object.prototype.hasOwnProperty.call(o, k) && pn === k.toLowerCase()) return o[k];
    }
  }
  return '';
}

function findWhere(list: any, k: string, v: string, caseSensitive = false) {
  const length = list.length;
  let o;
  for (let i = 0; i < length; i++) {
    o = prop(list[i], k, caseSensitive);
    if (
      typeof o !== 'undefined' &&
      ((caseSensitive && o === v) || o.toString().toLowerCase() === v.toString().toLowerCase())
    )
      return list[i];
  }
}

function getProductAttribute(product: any, attributeName: string) {
  return findWhere(product.properties.concat(product.options), 'attributeFQN', attributeName);
}

function getProductAttributeValues(product: any, attributeName: any, useNonStringValue: any): string {
  const attr: any = getProductAttribute(product, attributeName);
  const primitiveValues: any[] = [];
  const preferredValueProp = useNonStringValue ? 'value' : 'stringValue',
    secondaryValueProp = useNonStringValue ? 'stringValue' : 'value';
  if (attr) {
    const values = prop(attr, 'values', true);
    if (values) {
      for (let i = 0; i < values.length; i++) {
        primitiveValues[i] = prop(values[i], preferredValueProp, true) || prop(values[i], secondaryValueProp, true);
      }
      return '' + primitiveValues;
    }
  }

  return '';
}

function getProductAttributeFirstValue(product: any, attributeName: string, useNonStringValue: boolean): string {
  const values = getProductAttributeValues(product, attributeName, useNonStringValue);
  if (values) return values[0];

  return '';
}

Hypr.engine.setFilter('findwhere', findWhere);

Hypr.engine.setFilter('prop', prop);

Hypr.engine.setFilter('get_product_attribute', getProductAttribute);

Hypr.engine.setFilter('get_product_attribute_values', getProductAttributeValues);

Hypr.engine.setFilter('get_product_attribute_value', getProductAttributeFirstValue);

function createAscendingComparator(key: string) {
  return function (a: any, b: any) {
    if (a && b) {
      if (a[key] < b[key]) return -1;
      if (a[key] > b[key]) return 1;
    }
    return 0;
  };
}

function createDescendingComparator(key: string) {
  return function (a: any, b: any) {
    if (a && b) {
      if (a[key] > b[key]) return -1;
      if (a[key] < b[key]) return 1;
    }
    return 0;
  };
}

function createDictSortFilter(comparator?: (key: string) => (a: any, b: any) => number) {
  return function (dictList: any, key: string): string {
    const sorted = dictList.slice();

    // peek for the proper casing
    key = key.toLowerCase();
    for (const i in sorted[0]) {
      // eslint-disable-next-line no-prototype-builtins
      if (sorted[0].hasOwnProperty(i) && i.toLowerCase() === key) {
        key = i;
        break;
      }
    }

    if (comparator) {
      sorted.sort(comparator(key));
    }

    return sorted;
  };
}

Hypr.engine.setFilter('dictsort', createDictSortFilter(createAscendingComparator));

Hypr.engine.setFilter('dictsortreversed', createDictSortFilter(createDescendingComparator));

interface TimeSpan {
  TotalDays: number;
  Days: number;
  TotalHours: number;
  Hours: number;
  TotalMinutes: number;
  Minutes: number;
}

const TimeSpan = (function (this: TimeSpan, date: number) {
  if (!(this instanceof TimeSpan)) return new TimeSpan(date);
  const totalSeconds = date < 0 ? 0 : date / 1000;
  this.TotalDays = totalSeconds / 86400;
  this.Days = Math.floor(this.TotalDays);
  this.TotalHours = totalSeconds / 3600;
  this.Hours = Math.floor(this.TotalHours) % 24;
  this.TotalMinutes = totalSeconds / 60;
  this.Minutes = Math.floor(this.TotalMinutes) % 60;
} as any) as { new (date: number): TimeSpan };

function printDatePart(total: number, value: number, denom: string) {
  if (total < 1) return '';
  return value + ' ' + denom + (value !== 1 ? 's' : '');
}

type DatePart = {
  total: number;
  rounded: number;
  remaining: number;
  denom: string;
};

function getPart(timespan: TimeSpan, daysLeft: number, divider: number): DatePart {
  return {
    total: timespan.Days / divider,
    rounded: Math.floor(daysLeft / divider),
    remaining: daysLeft % divider,
    denom: '',
  };
}

function toHumanDate(memo: any, datePart: DatePart) {
  if (memo.elemsCount == 2) return memo;
  const strPart = printDatePart(datePart.total, datePart.rounded, datePart.denom);
  let elemsCount = memo.elemsCount;
  let space = '';
  if (strPart) {
    elemsCount = elemsCount + 1;
    if (memo.humanized) {
      space = ' ';
    }
  }
  return {
    humanized: memo.humanized + space + strPart,
    elemsCount: elemsCount,
  };
}

function timeBetween(date: number, laterDate: number) {
  if (!date || !laterDate) return '0 minutes';
  const timespan = new TimeSpan(laterDate - date);

  const yearPart = getPart(timespan, timespan.Days, 365);
  yearPart.denom = 'year';
  const monthPart = getPart(timespan, yearPart.remaining, 30);
  monthPart.denom = 'month';
  const weekPart = getPart(timespan, monthPart.remaining, 7);
  weekPart.denom = 'week';

  const parts: DatePart[] = [
    yearPart,
    monthPart,
    weekPart,
    {
      total: timespan.TotalDays,
      rounded: timespan.Days % 7,
      denom: 'day',
      remaining: 0,
    },
    {
      total: timespan.TotalHours,
      rounded: timespan.Hours,
      denom: 'hour',
      remaining: 0,
    },
    {
      total: timespan.TotalMinutes,
      rounded: timespan.Minutes,
      denom: 'minute',
      remaining: 0,
    },
  ];

  const resultDate = parts.reduce(toHumanDate, {
    humanized: '',
    elemsCount: 0,
  });
  if (resultDate.elemsCount === 0) return '0 minutes';
  return resultDate.humanized;
}

Hypr.engine.setFilter(
  'timeuntil',
  ensureDate(function (value: number, laterDate: number) {
    return timeBetween(value, laterDate);
  }),
);

Hypr.engine.setFilter(
  'timesince',
  ensureDate(function (value: number, laterDate: number) {
    return timeBetween(laterDate, value);
  }),
);

Hypr.engine.setFilter(
  'is_after',
  ensureDate(function (value: number, date: number) {
    return value > date;
  }),
);

Hypr.engine.setFilter(
  'is_before',
  ensureDate(function (value: number, date: number) {
    return date > value;
  }),
);

Hypr.engine.setFilter('parse_date', function (value: any): string {
  if (typeof value === 'string') {
    const n = Number(value);
    if (!isNaN(n)) return `${n}`;
  }
  return `${new Date(value).getTime() / 1000}`;
});

Hypr.engine.setFilter('add_time', function (value: any, moreTime: any) {
  value = MaybeDate(value);
  moreTime = parseInt(`${moreTime}`);
  if (!value) return '';
  if (!moreTime || isNaN(moreTime)) return value;
  return (+value + moreTime * 1000) / 1000;
});

Hypr.engine.setFilter('split', function (value: any, separator: string) {
  const sepType = typeof separator;
  const valType = typeof value;
  if (separator && sepType !== 'string' && sepType !== 'number') {
    throw new Error('Must supply a string or number as the separator to the |split filter.');
  }
  if (value && valType !== 'string' && valType !== 'number') {
    throw new Error('Must supply a string or number as the value to the |split filter.');
  }
  try {
    return value.toString().split(arguments.length === 2 ? separator.toString() : ' ');
  } catch (e) {
    throw new Error('Error in |split filter: ' + e);
  }
});

Hypr.engine.setFilter(
  'replace',
  function (value: number | string, toReplace: number | string, replacement: number | string) {
    const toReplaceType = typeof toReplace;
    const replacementType = typeof replacement;
    const valType = typeof value;
    if (value && valType !== 'string' && valType !== 'number') {
      throw new Error('Must supply a string or number as the value to the |replace filter.');
    }
    if (arguments.length === 1) {
      throw new Error('Must call |replace filter with at least one argument.');
    }
    if (toReplace && toReplaceType !== 'string' && toReplaceType !== 'number') {
      throw new Error('Must supply a string or number as the string to replace argument to the |replace filter.');
    }
    if (replacement && replacementType !== 'string' && replacementType !== 'number') {
      throw new Error('Must supply a string or number as the second argument to the |split filter.');
    }
    try {
      return value
        .toString()
        .split(toReplace.toString())
        .join(replacement ? replacement.toString() : '');
    } catch (e) {
      throw new Error('Error in |replace filter: ' + e);
    }
  },
);

new UnSupported().register(Hypr);
new Comment().register(Hypr);
new Dump().register(Hypr);
new With().register(Hypr);

// End of code extracted from Kibo's HyperLive file

/**
 * Hypr File specific tags & filters - This is pretty much WIP
 * along the way if we find any tag which is used pretty ofter then
 * we will add that tag in this file.
 * But the sole purpose of crating this file is serving hypr file locally for kibo-react-core theme;
 * and we recommend developer's to use the React for doing most of the operations
 * we been doing in hypr file with the help of tags & filter.
 *
 * At any-point in future if Kibo provides and API to get all the data like customer JSON
 * then we will make this file obsolete and will use the API's only and get rid of parsing hyper files.
 * Till then consider this file as ony handy utility.
 */

new PreloadJSON().register(Hypr);
new RequireScript().register(Hypr);
new FirstOf().register(Hypr);
new PartialCache().register(Hypr);
new Dropzone().register(Hypr);

export default Hypr;
