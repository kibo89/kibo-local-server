/* eslint-disable @typescript-eslint/no-explicit-any */
import SwigTag from './SwigTag';
import { Hypr } from '../HyprTypes';

export default class FirstOf implements SwigTag {
  register(hypr: Hypr): void {
    hypr.engine.setTag('firstof', this.parse, this.compile);
  }

  parse(): boolean {
    return true;
  }

  compile(
    compiler?: (content?: string, parents?: any, options?: any, blockName?: string) => string,
    args?: any[],
  ): string {
    return '(function() {_output += ' + args?.join(' || ') + ' || "";\n})();';
  }
}
