import { lexer } from 'swig';
import { Hypr } from '../HyprTypes';
import SwigTag from './SwigTag';

export default class Dump implements SwigTag {
  emitter = [
    ';(function(v, n) { ',
    '_output += "<pre class=\\"hypr-dump\\"><code>"; ',
    '_output += Object.prototype.toString.call(v) + n; ',
    "if (typeof v === 'object') { ",
    'try { ',
    '_output += _filters.e(JSON.stringify(v, null, 2)) + n; ',
    '} catch(e) { _output += "Error: Could not serialize." + n; } ',
    '} else { ',
    '_output += _filters.e(v) + n; ',
    '} ',
    '_output += "</code></pre>"; ',
    '}({0}, "\\n"));',
  ]
    .join('')
    .split('{0}');

  register(this: Dump, hypr: Hypr): void {
    hypr.engine.setTag('dump', this.parse, this.compile);
  }

  // eslint-disable-next-line @typescript-eslint/no-explicit-any,@typescript-eslint/explicit-module-boundary-types
  parse(str?: string, line?: string, parser?: any): boolean {
    parser.on(lexer.TYPES.VAR, function () {
      return true;
    });

    return true;
  }

  compile(
    this: Dump,
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    compiler?: (content?: string, parents?: any, options?: any, blockName?: string) => string,
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    args?: any[],
  ): string {
    let o = '';
    const l = args ? args.length : 0;

    for (let i = 0; i < l; i++) {
      o += this.emitter.join(args ? args[i] : '');
    }

    return o;
  }
}
