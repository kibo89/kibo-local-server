import SwigTag from './SwigTag';
import { Hypr } from '../HyprTypes';

export default class Dropzone implements SwigTag {
  format = ['\n_output += "<div id=\\"mz-drop-zone-', '\\" class=\\"mz-drop-zone\\"></div>";'];

  register(this: Dropzone, hypr: Hypr): void {
    hypr.engine.setTag('dropzone', this.parse, this.compile, false, false);
  }

  // eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types,@typescript-eslint/no-explicit-any
  compile(this: Dropzone, compiler?: any, args?: string[]): string {
    const dropzoneName: string = args?.shift() || '';
    return this.format.join(dropzoneName.substring(1, dropzoneName.length - 1));
  }

  // eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types,@typescript-eslint/no-explicit-any
  parse(str?: string, line?: string, parser?: any, types?: any): boolean {
    let named = false;
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    parser.on(types.STRING, function (this: any, token: any) {
      if (!named) {
        this.out.push(token.match);
        named = true;
        return true;
      }
      return false;
    });
    return true;
  }
}
