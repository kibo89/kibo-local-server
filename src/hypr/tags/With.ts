import SwigTag from './SwigTag';
import { Hypr } from '../HyprTypes';
import { lexer } from 'swig';

export default class With implements SwigTag {
  as = 'as';
  asError = 'The {% with %} tag requires the token "as" to appear once and only once.';

  register(this: With, hypr: Hypr): void {
    hypr.engine.setTag('with', this.parse, this.compile, true, false);
  }

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  compile(
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    compiler?: (content?: string, parents?: any, options?: any, blockName?: string) => string,
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    args?: any[],
    content?: string,
    // eslint-disable-next-line @typescript-eslint/no-explicit-any,@typescript-eslint/explicit-module-boundary-types
    parents?: any,
    // eslint-disable-next-line @typescript-eslint/no-explicit-any,@typescript-eslint/explicit-module-boundary-types
    options?: any,
  ): string {
    const localVar = args?.pop();
    const argsJoin = args ? args.join('') : '';
    return compiler
      ? '\n(function(' + localVar + '){\n' + compiler(content, parents, options) + ';\n})(' + argsJoin + ');\n'
      : argsJoin;
  }

  // eslint-disable-next-line @typescript-eslint/no-explicit-any,@typescript-eslint/explicit-module-boundary-types
  parse(this: With, str?: string, line?: string, parser?: any): boolean {
    // eslint-disable-next-line @typescript-eslint/no-this-alias
    const me = this;
    let asEncountered = false;
    parser.on('*', function () {
      if (!asEncountered) return true;
    });
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    parser.on(lexer.TYPES.VAR, function (this: any, token: any) {
      if (token.match === me.as) {
        if (asEncountered) throw new Error('Error on line ' + line + ': ' + me.asError);
        asEncountered = true;
        return false;
      } else if (this.prevToken && this.prevToken.match === me.as) {
        this.out.push(token.match);
      } else {
        return true;
      }
    });
    parser.on('end', function () {
      if (!asEncountered) throw new Error('Error on line ' + line + ': ' + me.asError);
    });

    return true;
  }
}
