import { Hypr } from '../HyprTypes';
import SwigTag from './SwigTag';

export default class Comment implements SwigTag {
  register(this: Comment, hypr: Hypr): void {
    hypr.engine.setTag('comment', this.parse, this.compile, true);
  }

  compile(): string {
    return '';
  }

  // eslint-disable-next-line @typescript-eslint/no-explicit-any,@typescript-eslint/explicit-module-boundary-types
  parse(str?: string, line?: string, parser?: any): boolean {
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    parser?.on('*', function (token: any) {
      throw new Error('Unexpected token "' + token.match + '" on line ' + line + '.');
    });

    return true;
  }
}
