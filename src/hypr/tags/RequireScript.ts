/* eslint-disable @typescript-eslint/no-explicit-any */
import { Hypr } from '../HyprTypes';
import path from 'path';
import fs from 'fs';
import SwigTag from './SwigTag';

class RequireScript implements SwigTag {
  register(this: RequireScript, hypr: Hypr): void {
    hypr.engine.setTag('require_script', this.parse, this.compile);
  }

  parse(): boolean {
    return true;
  }

  compile(
    compiler?: (content?: string, parents?: any, options?: any, blockName?: string) => string,
    args?: any[],
  ): string {
    const hyprCtxVar = `${args?.pop()}`;
    let scriptDir = '';
    const strNormalizerRgx = new RegExp('"', 'g');

    const kiboHostedJsFilePath = path.join(
      __dirname,
      '...',
      '..',
      '..',
      'kibo-hosted-resources',
      ...`${hyprCtxVar.replace(strNormalizerRgx, '')}.js`.split('/'),
    );

    const localHostedJsFilePath = path.join(
      process.cwd(),
      'kibo-core',
      'scripts',
      ...`${hyprCtxVar.replace(strNormalizerRgx, '')}.js`.split('/'),
    );
    if (fs.existsSync(kiboHostedJsFilePath)) {
      scriptDir = `${path.sep}hscripts${path.sep}${hyprCtxVar.replace(strNormalizerRgx, '')}.js`;
    } else if (fs.existsSync(localHostedJsFilePath)) {
      scriptDir = `${path.sep}lscripts${path.sep}${hyprCtxVar.replace(strNormalizerRgx, '')}.js`;
    }

    if (scriptDir && scriptDir !== '') {
      return '(function(){_output += `<script src="' + scriptDir + '"></script>`; })();';
    }

    return '';
  }
}

export default RequireScript;
