import SwigTag from './SwigTag';
import { Hypr } from '../HyprTypes';

export default class PartialCache implements SwigTag {
  register(this: PartialCache, hypr: Hypr): void {
    hypr.engine.setTag('partial_cache', this.parse, this.compile, true, true);
  }

  compile(
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    compiler?: (content?: string, parents?: any, options?: any, blockName?: string) => string,
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    args?: any[],
    // eslint-disable-next-line @typescript-eslint/no-explicit-any,@typescript-eslint/explicit-module-boundary-types
    content?: any,
    // eslint-disable-next-line @typescript-eslint/no-explicit-any,@typescript-eslint/explicit-module-boundary-types
    parents?: any,
    // eslint-disable-next-line @typescript-eslint/no-explicit-any,@typescript-eslint/explicit-module-boundary-types
    options?: any,
  ): string {
    let contentToForward = content;

    if (content instanceof Array) {
      contentToForward = content.filter((contentEl) => !(typeof contentEl === 'string' && contentEl === '\n'));
    }

    return compiler ? compiler(contentToForward, parents, options, args?.join('')) : '';
  }

  parse(): boolean {
    return true;
  }
}
