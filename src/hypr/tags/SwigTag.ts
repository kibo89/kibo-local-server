/* eslint-disable @typescript-eslint/no-explicit-any */
import { lexer, Swig } from 'swig';
import { Hypr } from '../HyprTypes';

export default interface SwigTag {
  parse: (
    str?: string,
    line?: string,
    parser?: any,
    types?: lexer.TYPES,
    stack?: any,
    opts?: any,
    swig?: Swig,
  ) => boolean;
  compile: (
    compiler?: (content?: string, parents?: any, options?: any, blockName?: string) => string,
    args?: any[],
    content?: string,
    parents?: any,
    options?: any,
    blockName?: string,
  ) => string;

  register: (hypr: Hypr) => void;
}
