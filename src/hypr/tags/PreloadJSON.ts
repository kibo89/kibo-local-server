/* eslint-disable @typescript-eslint/no-explicit-any */
import fs from 'fs';
import path from 'path';
import { Hypr } from '../HyprTypes';
import SwigTag from './SwigTag';

class PreloadJSON implements SwigTag {
  register(this: PreloadJSON, hypr: Hypr): void {
    hypr.engine.setTag('preload_json', this.parse, this.compile);
  }

  parse(): boolean {
    return true;
  }

  compile(
    compiler?: (content?: string, parents?: any, options?: any, blockName?: string) => string,
    args?: any[],
  ): string {
    const hyprCtxVar = `${args?.pop()}`;
    const preloadObj = args?.pop();
    let preloadObjFromDir = '{}';
    const strNormalizerRgx = new RegExp('"', 'g');

    const preloadFilePath = path.join(
      __dirname,
      '..',
      '..',
      '..',
      'preloads',
      `${hyprCtxVar.replace(strNormalizerRgx, '')}.json`,
    );
    if (fs.existsSync(preloadFilePath)) {
      preloadObjFromDir = fs.readFileSync(preloadFilePath).toString();
    }

    const swigTplTxt =
      '(function(hyprCtxVar){ try {' +
      '_output += `<script type="text/json" id="data-mz-preload-${hyprCtxVar}"> ${JSON.stringify(' +
      preloadObj +
      ' || ' +
      preloadObjFromDir +
      ')}</script>`' +
      '} catch(err) { _output += "Error: Could not serialize."; } })' +
      '(' +
      hyprCtxVar +
      ');';

    return compiler ? swigTplTxt : '';
  }
}

export default PreloadJSON;
