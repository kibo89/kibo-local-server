import { Hypr } from '../HyprTypes';

export default class UnSupportedTags {
  register(this: UnSupportedTags, hypr: Hypr): void {
    const nullTags = ['json_attribute', 'data_attributes'];
    for (let t = 0; t < nullTags.length; t++) {
      hypr.engine.setTag(
        nullTags[t],
        () => true,
        () => '',
        false,
        true,
      );
    }
  }
}
