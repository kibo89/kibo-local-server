/* eslint-disable @typescript-eslint/no-explicit-any */
import { TemplateLoader } from 'swig';
import fs from 'fs';
import path from 'path';

export default class LocalFileLoader implements TemplateLoader {
  load(identifier: string): any {
    return fs.readFileSync(identifier, 'utf8');
  }

  resolve(to: string): string {
    const basepath = path.join(process.cwd(), 'kibo-core', 'templates');

    let toFileWithExt = to;
    const absFilePath = `${basepath}${path.sep}${to}`;
    if (to === 'page' || to === 'page.hypr') {
      toFileWithExt = path.join(process.cwd(), 'build', 'index.html');
    } else {
      if (fs.existsSync(`${absFilePath}.hypr`)) {
        toFileWithExt = `${to}.hypr`;
      } else if (fs.existsSync(`${absFilePath}.hypr.live`)) {
        toFileWithExt = `${to}.hypr.live`;
      }
    }

    return path.resolve(basepath, toFileWithExt);
  }
}
